import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {storeData} from './ayncStorage';
import {ENABLE_LOGIN, SCOPE, ACCOUNT_NAME} from './Constants';

export function LoginScreen({navigation}) {
  const enableLogin = async (loginType: string) => {
    var formdata = new FormData();
    formdata.append('scope', SCOPE);
    formdata.append('accountName', ACCOUNT_NAME);

    var requestOptions = {
      method: 'POST',
      body: formdata,
      redirect: 'follow',
    };

    fetch(ENABLE_LOGIN, requestOptions)
      .then((response) => {
        // @ts-ignore
        const cookie = response.headers.map['set-cookie'];
        console.log('cookie NUEVA', cookie);
        storeData(cookie);

        navigation.navigate(loginType);
      })
      .catch((error) => console.log('error', error));
  };

  return (
    <View
      style={{flex: 1, alignItems: 'center', justifyContent: 'space-around'}}>
      <TouchableOpacity onPress={() => enableLogin('LoginByOTP')}>
        <Text>Iniciar sesión por otp</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => enableLogin('LoginByPassword')}>
        <Text>Iniciar sesión por constaseña</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => enableLogin('SignIn')}>
        <Text>Registrarse</Text>
      </TouchableOpacity>
    </View>
  );
}
