import React, {useState} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import {getData} from '../ayncStorage';
import {CREATE_OR_CHANGE_PASSWORD, SEND_OTP_CODE} from '../Constants';

export function SignInScreen({navigation}: any) {
  const [email, setEmail] = useState('santiago.vanegas@vtex.com.br');
  const [password, setPassword] = useState('');
  const [accessKey, setAccessKey] = useState('');

  const sendOtpCode = () => {
    getData().then(async (cookie) => {
      var myHeaders = new Headers();
      myHeaders.append('Cookie', cookie);

      var formdata = new FormData();
      formdata.append('email', email);

      fetch(SEND_OTP_CODE, {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow',
      })
        .then((response) => response.text())
        .then((result) => console.log(result))
        .catch((error) => console.log('error', error));
    });
  };

  const login = async () => {
    getData().then(async (cookie) => {
      var myHeaders = new Headers();
      myHeaders.append('Cookie', cookie);

      var formdata = new FormData();
      formdata.append('email', email);
      formdata.append('newPassword', password);
      formdata.append('accessKey', accessKey);

      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow',
      };

      fetch(CREATE_OR_CHANGE_PASSWORD, requestOptions)
        .then((response) => response.text())
        .then((result) => console.log(result))
        .catch((error) => console.log('error', error));
    });
  };

  return (
    <View
      style={{flex: 1, alignItems: 'center', justifyContent: 'space-around'}}>
      <TextInput
        placeholder="Email"
        value={email}
        onChangeText={(txt) => setEmail(txt)}
      />

      <TouchableOpacity onPress={sendOtpCode}>
        <Text>Enviar Código</Text>
      </TouchableOpacity>

      <TextInput
        placeholder="Nueva Constraseña"
        value={password}
        onChangeText={(txt) => setPassword(txt)}
      />
      <TextInput
        placeholder="Código"
        value={accessKey}
        onChangeText={(txt) => setAccessKey(txt)}
      />

      <TouchableOpacity onPress={login}>
        <Text>Registrarse</Text>
      </TouchableOpacity>
    </View>
  );
}
