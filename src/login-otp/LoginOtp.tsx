import React, {useState} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import {getData, storeData} from '../ayncStorage';
import {SEND_OTP_CODE, VALIDATE_OTP_CODE} from '../Constants';

export function LoginByOtpScreen({navigation}) {
  const [email, setEmail] = useState('santiago.vanegas@vtex.com.br');
  const [codigoOtp, setCodigoOtp] = useState('');

  const sendOtpCode = () => {
    getData().then(async (cookie) => {
      var myHeaders = new Headers();
      myHeaders.append('Cookie', cookie);

      var formdata = new FormData();
      formdata.append('email', email);

      fetch(SEND_OTP_CODE, {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow',
      })
        .then((response) => response.text())
        .then((result) => console.log(result))
        .catch((error) => console.log('error', error));
    });
  };

  const validateOtp = async () => {
    getData().then(async (cookie) => {
      console.log('codigoOtp', codigoOtp);

      var myHeaders = new Headers();
      myHeaders.append('Cookie', cookie);

      var formdata = new FormData();
      formdata.append('email', email);
      formdata.append('accesskey', codigoOtp);

      fetch(VALIDATE_OTP_CODE, {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow',
      })
        .then((response) => response.text())
        .then((result) => console.log(result))
        .catch((error) => console.log('error', error));
    });
  };

  return (
    <View
      style={{flex: 1, alignItems: 'center', justifyContent: 'space-around'}}>
      <TextInput
        placeholder="Escriba su email"
        value={email}
        onChangeText={(txt) => setEmail(txt)}
      />

      <TouchableOpacity onPress={sendOtpCode}>
        <Text>Enviar Código</Text>
      </TouchableOpacity>

      <TextInput
        onChangeText={(txt) => setCodigoOtp(txt)}
        value={codigoOtp}
        placeholder="Ingrese el código de acceso"
      />

      <TouchableOpacity onPress={validateOtp}>
        <Text>Enviar Código</Text>
      </TouchableOpacity>
    </View>
  );
}
