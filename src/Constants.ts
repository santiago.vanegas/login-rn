const HOST_NAME = 'vtexcol.myvtex.com';
export const ENABLE_LOGIN = `https://${HOST_NAME}/api/vtexid/pub/authentication/startlogin`;
export const SEND_OTP_CODE = `https://${HOST_NAME}/api/vtexid/pub/authentication/accesskey/send`;
export const VALIDATE_OTP_CODE = `https://${HOST_NAME}/api/vtexid/pub/authentication/accesskey/validate`;
export const LOGIN_WITH_PASSWORD = `https://${HOST_NAME}/api/vtexid/pub/authentication/classic/validate`;
export const CREATE_OR_CHANGE_PASSWORD = `https://${HOST_NAME}/api/vtexid/pub/authentication/classic/setpassword`;
export const SCOPE = 'vtexcol';
export const ACCOUNT_NAME = 'vtexcol';
