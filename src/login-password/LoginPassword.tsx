import React, {useState} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import {getData} from '../ayncStorage';
import {LOGIN_WITH_PASSWORD} from '../Constants';

export function LoginByPassword({navigation}: any) {
  const [email, setEmail] = useState('santiago.vanegas@vtex.com.br');
  const [password, setPassword] = useState('');

  const login = async () => {
    getData().then(async (cookie) => {
      var myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
      myHeaders.append('Cookie', cookie);

      var formdata = new FormData();
      formdata.append('login', email);
      formdata.append('password', password);

      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow',
      };

      fetch(LOGIN_WITH_PASSWORD, requestOptions)
        .then((response) => response.text())
        .then((result) => console.log(result))
        .catch((error) => console.log('error', error));
    });
  };

  return (
    <View
      style={{flex: 1, alignItems: 'center', justifyContent: 'space-around'}}>
      <TextInput
        placeholder="Email"
        value={email}
        onChangeText={(txt) => setEmail(txt)}
      />
      <TextInput
        placeholder="Constraseña"
        value={password}
        onChangeText={(txt) => setPassword(txt)}
      />

      <TouchableOpacity onPress={login}>
        <Text>Entrar</Text>
      </TouchableOpacity>
    </View>
  );
}
