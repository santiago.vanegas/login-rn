import AsyncStorage from '@react-native-async-storage/async-storage';

export const storeData = async (value) => {
  try {
    await AsyncStorage.setItem('@storage_Key', value);
  } catch (e) {
    console.error(e);
  }
};

export const getData = async () => {
  try {
    return await AsyncStorage.getItem('@storage_Key');
  } catch (e) {
    console.error(e);
  }
};
