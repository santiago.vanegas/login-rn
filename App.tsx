import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {LoginByOtpScreen} from './src/login-otp/LoginOtp';
import {LoginScreen} from './src/Login';
import {LoginByPassword} from './src/login-password/LoginPassword';
import {SignInScreen} from './src/sign-in/SignIn';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="LoginByOTP" component={LoginByOtpScreen} />
        <Stack.Screen name="LoginByPassword" component={LoginByPassword} />
        <Stack.Screen name="SignIn" component={SignInScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
